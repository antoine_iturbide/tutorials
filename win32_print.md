# How to print using the Windows API

Refferences:

`#include <windows.h>` :
[`AttachConsole`](https://docs.microsoft.com/en-us/windows/console/attachconsole)
[`WriteFile`](https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-writefile)

`#include <stdio.h>` :
[`freopen`](https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/freopen-wfreopen)
[`printf`](https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/printf-printf-l-wprintf-wprintf-l)

## Minimal Hello World
Displayed in calling process's console.
```c
#include <windows.h>

int WinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR     lpCmdLine,
    int       nShowCmd
) {
    AttachConsole(ATTACH_PARENT_PROCESS);
    WriteFile(
        GetStdHandle(STD_OUTPUT_HANDLE),
        "Hello;Sailor!\n",
        lstrlenA("Hello;Sailor!\n"),
        NULL,
        NULL
    );
}
```

## Minimal print() procedure
```c
#include <windows.h>

void print(char* msg) {
    AttachConsole(ATTACH_PARENT_PROCESS);
    WriteFile(
        GetStdHandle(STD_OUTPUT_HANDLE),
        msg,
        lstrlenA(msg),
        NULL,
        NULL
    );
}

int WinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR     lpCmdLine,
    int       nShowCmd
) {
    print("Hello;Sailor!");
}
```
Despite this working fine, you probably want to call `AttachConsole(ATTACH_PARENT_PROCESS)` and `GetStdHandle(STD_OUTPUT_HANDLE)` only once at the start of your application instead.

## Rewiring printf() to calling process' console
```c
#include <windows.h>
#include <stdio.h> // printf, freopen

int WinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR     lpCmdLine,
    int       nShowCmd
) {
    // rewires printf output to calling process' console
    {
        AttachConsole(ATTACH_PARENT_PROCESS);
        freopen("CON", "w", stdout);
    }
    printf("Hello;Sailor!\n");
}

```
