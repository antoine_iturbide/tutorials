# Simplest JSP "Hello World" using Wildfly standalone (JBoss) on Windows
## I. Creating our project
1. First start by creating a **directory**. It will hold our WAR (**W**eb application **A**rchive) project.
   In this example, we will be using `hello_world`.
````
hello_world/
````
2. Within the project directory, create an **empty text file** with a `.jsp` extention.
   In this example, we will use `hello_world.jsp`
````
hello_world/
|- hello_world.jsp
````
3. Fill-up the content of the `hello_world.jsp` file as follows.[^1]
````
<html>
	<head>
		<title>Hello World.</title>
	</head>
	<body>
		<h1>Hello World.</h1>
	</body>
</html>
````
4. Create a new directory **mendatory named** as `WEB-INF`.
````
hello_world/
|- WEB-INF/
|- hello_world.jsp
````
5. Within the `WEB-INF` directory, create a new empty text file **mendatory named** as `web.xml`.
````
hello_world/
|- WEB-INF/
|  |- web.xml
|- hello_world.jsp
````
6. Fill-up the content of the `web.xml` file as follows.
````
<web-app>
  <welcome-file-list>
          <welcome-file>hello_world.jsp</welcome-file>
  </welcome-file-list>	
</web-app>
````

## II. Exporting our project to a deployable `.war` file
1. Open a console in your project dirrectory.
2. Execute the folowing command : `jar -cvf hello_world.war *`[^2]
3. In the end your project folder should look like this.
````
hello_world/
|- WEB-INF/
|  |- web.xml
|- hello_world.jsp
|- hello_world.war <- just a zip file
   |- META-INF/
   |  |- MANIFEST.MF
   |- WEB-INF/
   |  |- web.xml
   |- hello_world.jsp
````

## III. Deploying our `.war` with Wildfly (JBoss)
### A. Running Wildfly on Windows
1. Download Wildfly server from: https://www.wildfly.org/downloads/
2. Extract it and run `bin/standalone.bat`
3. Check your server instance is correctly running by accessing http://localhost:8080/
### B. Deploying our `.war`
1. Access your server controll pannel here : http://localhost:9990/
   1.a. In case you get a message telling you there is no configured users. **From a console**, run `add-user.bat` and folow the instructions to add new user. You can name it `root` and set the password as `root`, _getting a nice warning about how such username and passwords are quite unsecure._
   1.b. Try to re-access http://localhost:9990/ (`Ctrl+F5`, or reboot the server).
5. Go to the deployement pannel.
6. Click on the `(+)` icon and select _Upload Deployement_.
7. Feed your `.war` file.
8. Cick next twice. _You can leave everything as default._
9. Your `wello_world.jsp` file should now be accessible at http://localhost:8080/hello_world/.

[^1]: As you can see this is just plain `html`, no embeded java code yet.
[^2]: `jar` is a program from the Java Developement Kit. In this instance, all it does is ziping your project, naming it `.war` instead of `.zip` and adding a `META-INF/MANIFEST.MF` file within the archive.
